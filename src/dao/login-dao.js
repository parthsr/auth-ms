const md5 = require('md5')
const logger = require('../trace/logger')
const db = require('../models')

async function getUser(requestBody) {
  try {
    const { email, password } = requestBody
    logger.info('fetching user details for %s from database', email)
    const userDetails = await db.signups.findOne({
      attributes: ['firstName', 'lastName', 'email', 'password', 'gender', 'id'],
      where: {
        email
      }
    });
    if (userDetails && userDetails.dataValues) {
      if (userDetails.dataValues.password === md5(password)) {
        logger.info('Successfully fetched user details from database')
        return {
          successfulOperation: true,
          value: userDetails.dataValues
        }
      }
      logger.info('password incorrect. Please reenter')
      return {
        successfulOperation: false,
        value: 'password incorrect. Please reenter'
      }
    }
    const error = 'User does not exist. Please SignUp'
    logger.info('Received error while fetching user details %j', error)
    return {
      successfulOperation: false,
      value: error
    }
  } catch (err) {
    logger.error('Received error while fetching user details %j', err)
    return {
      successfulOperation: false,
      value: err.message
    }
  }
}

module.exports = {
  getUser,
}
