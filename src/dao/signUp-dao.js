const md5 = require('md5')
const logger = require('../trace/logger')
const db = require('../models')

async function createUser(requestBody) {
  try {
    const {
      firstName, lastName, email, phone, gender, password
    } = requestBody
    const userObject = {
      firstName, lastName, email, phone, gender, password: md5(password)
    }
    logger.info('Creating new user record in database')
    const userDetails = await db.signups.create(userObject);
    logger.info('Successfully created user record for %s in database', email)
    return {
      successfulOperation: true,
      value: userDetails.dataValues
    }
  } catch (err) {
    logger.error('Received error while creating user record in database %j', err)
    return {
      successfulOperation: false,
      value: err.message
    }
  }
}

module.exports = {
  createUser,
}
