

module.exports = (sequelize, DataTypes) => {
  const signup = sequelize.define('signups', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    gender: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    classMethods: {
    }
  });
  return signup;
};
