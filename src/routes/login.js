
const express = require('express');
const jwt = require('jsonwebtoken')
const logger = require('../trace/logger');
const { isLoginJoiValid } = require('../helpers/joiValidations')
const { getUser } = require('../dao/login-dao')


const router = express.Router();


router.get('/', async (req, res) => {
  logger.info('Received request for login');
  const { email, password } = req.headers
  const requestObject = {
    email, password
  }
  const joiResponse = isLoginJoiValid(requestObject)
  if (!joiResponse.valid) {
    logger.debug('User object violates schema rules \n%s\n%s', joiResponse.error, JSON.stringify(requestObject, null, 2))
    res.status(422).json({
      status: 'error',
      message: joiResponse.error,
      requestObject
    });
    return
  }

  const getUserFromDb = await getUser(requestObject)
  if (getUserFromDb.successfulOperation) {
    jwt.sign({ data: getUserFromDb.value }, 'helloWorld', { expiresIn: 30 * 60 }, (err, token) => {
      res.json({
        token
      });
    })
  } else {
    if (getUserFromDb.value.trim() === 'Validation error') {
      res.status(400).send(`User already exists. Please login. ${getUserFromDb.value}`)
    } else {
      res.status(400).send(getUserFromDb.value)
    }
    logger.debug('User object was not inserted into database')
  }
});

module.exports = router;
