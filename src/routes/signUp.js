
const express = require('express');
const jwt = require('jsonwebtoken')
const logger = require('../trace/logger');
const { createUser } = require('../dao/signUp-dao')
const { isSignUpJoiValid } = require('../helpers/joiValidations')


const router = express.Router();


router.post('/', async (req, res) => {
  logger.info('Received request for signup');
  const body = req.body
  const joiResponse = isSignUpJoiValid(body)
  if (!joiResponse.valid) {
    logger.debug('User object violates schema rules \n%s\n%s', joiResponse.error, JSON.stringify(body, null, 2))
    res.status(422).json({
      status: 'error',
      message: joiResponse.error,
      body
    });
    return
  }
  const insertIntoDb = await createUser(req.body)
  if (insertIntoDb.successfulOperation) {
    jwt.sign({ data: insertIntoDb.value }, 'helloWorld', { expiresIn: 30 * 60 }, (err, token) => {
      res.json({
        token
      });
    })
  } else {
    if (insertIntoDb.value === 'Validation error') {
      res.status(400).send('user already exists')
      logger.debug('user already exists')
    }
    res.status(400).send(insertIntoDb.value)
    logger.debug('User object was not inserted into database')
  }
});

module.exports = router;
