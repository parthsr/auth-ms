const express = require('express');
// const jwt = require('jsonwebtoken')
const logger = require('../trace/logger');

const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  logger.info('Received request for getting home page');
  res.send('respond with a resource');
});

module.exports = router;
