const express = require('express');
const jwt = require('jsonwebtoken')
const logger = require('../trace/logger');

const router = express.Router();

router.get('/', (req, res) => {
  logger.info('Received request for login');
  const { authorization } = req.headers;
  const authToken = authorization.split(' ')[1]
  jwt.verify(authToken, 'helloWorld', (err) => {
    if (err) {
      res.status(403).send(err.message)
    } else {
      res.status(200).send('ok')
    }
  })
});

module.exports = router;
