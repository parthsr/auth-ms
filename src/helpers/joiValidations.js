const Joi = require('joi')

const isSignUpJoiValid = (userObject) => {
  const schema = Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    phone: Joi.string().regex(/^\d{3}-\d{3}-\d{4}$/).required(),
    gender: Joi.string(),
    password: Joi.string().required()
  });

  const validationResult = Joi.validate(userObject, schema)
  if (validationResult.error) {
    return {
      valid: false,
      error: validationResult.error.message
    }
  }
  return {
    valid: true
  }
}
const isLoginJoiValid = (userObject) => {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required()
  });

  const validationResult = Joi.validate(userObject, schema)
  if (validationResult.error) {
    return {
      valid: false,
      error: validationResult.error.message
    }
  }
  return {
    valid: true
  }
}


module.exports = {
  isSignUpJoiValid,
  isLoginJoiValid
};
