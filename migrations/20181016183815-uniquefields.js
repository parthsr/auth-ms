

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addConstraint('signups', ['email'], {
      type: 'unique',
      name: 'emailUnique',
    });
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeConstraint('signups', 'emailUnique');
  }
};
